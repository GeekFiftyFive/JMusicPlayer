import java.net.URI;
import java.util.concurrent.TimeUnit;

import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.freedesktop.gstreamer.Bus;
import org.freedesktop.gstreamer.ClockTime;
import org.freedesktop.gstreamer.ElementFactory;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.GstObject;
import org.freedesktop.gstreamer.State;
import org.freedesktop.gstreamer.elements.PlayBin;

import javafx.util.Duration;

public class GStreamerMusicPlayer implements GenericMusicPlayer{
	private PlayBin playbin;
	private Runnable onReady;
	private Runnable onEnd;
	
	public GStreamerMusicPlayer(URI uri) {
		playbin = new PlayBin("AudioPlayer");
		playbin.setVideoSink(ElementFactory.make("fakesink", "videosink"));
		playbin.setURI(uri);
		playbin.getBus().connect((Bus.EOS) obj -> onEnd.run());
		playbin.getBus().connect(new Bus.STATE_CHANGED() {
			
			@Override
			public void stateChanged(GstObject obj, State state1, State state2, State transition) {
				if(onReady != null && state2 == State.READY) onReady.run();
			}
		});
		playbin.setState(State.READY);
	}
	
	public GStreamerMusicPlayer() {
		
	}

	@Override
	public void play() {
		playbin.play();
	}

	@Override
	public void pause() {
		playbin.pause();
	}

	@Override
	public Duration getTotalDuration() {
		int duration = (int) playbin.queryDuration(TimeUnit.SECONDS);
		return Duration.seconds(duration);
	}

	@Override
	public Duration getCurrentDuration() {
		int duration = (int) playbin.queryPosition(TimeUnit.SECONDS);
		return Duration.seconds(duration);
	}

	@Override
	public void setCurrentDuration(Duration duration) {
		ClockTime newPos = ClockTime.fromSeconds((long) duration.toSeconds());
		playbin.seek(newPos);
	}

	@Override
	public void close() {
		playbin.setState(State.NULL);
		Gst.quit();
	}

	@Override
	public void setOnReady(Runnable runnable) {
		onReady = runnable;
	}

	@Override
	public void setOnEndOfMedia(Runnable runnable) {
		onEnd = runnable;
	}

	@Override
	public PlayerState getState() {
		State state = playbin.getState();
		if(state == State.PLAYING) return PlayerState.PLAYING;
		else if(state == State.PAUSED) return PlayerState.PAUSED;
		else return PlayerState.INACTIVE;
	}

	@Override
	public FileFilter getFilter() {
		return new FileNameExtensionFilter("Audio Files", "wav", "mp3", "aac", "flac");
	}

}
