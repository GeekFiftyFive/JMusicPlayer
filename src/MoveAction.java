
public class MoveAction extends PlayerRunnable {
	int delta;
	
	public MoveAction(JMusicPlayer player, int delta) {
		super(player);
		this.delta = delta;
	}

	@Override
	public void run() {
		player.moveTrack(delta);
	}

}
