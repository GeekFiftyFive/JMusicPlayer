
public class MoveTrackAction extends PlayerRunnable {
	private int delta;

	public MoveTrackAction(JMusicPlayer player, int delta) {
		super(player);
		this.delta = delta;
	}

	@Override
	public void run() {
		player.moveInPlaylist(delta);
	}

}
