import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class JavaFXMusicPlayer implements GenericMusicPlayer {
	MediaPlayer fxPlayer;
	
	public JavaFXMusicPlayer(Media track) {
		fxPlayer = new MediaPlayer(track);
	}
	
	public JavaFXMusicPlayer() {
		
	}

	@Override
	public void play() {
		fxPlayer.play();
	}

	@Override
	public void pause() {
		fxPlayer.pause();
	}

	@Override
	public Duration getTotalDuration() {
		return fxPlayer.getTotalDuration();
	}

	@Override
	public Duration getCurrentDuration() {
		return fxPlayer.getCurrentTime();
	}

	@Override
	public void setCurrentDuration(Duration duration) {
		fxPlayer.seek(duration);
	}
	
	@Override
	public void close() {
		fxPlayer.dispose();
	}
	
	@Override
	public void setOnReady(Runnable runnable) {
		fxPlayer.setOnReady(runnable);
	}

	@Override
	public void setOnEndOfMedia(Runnable runnable) {
		fxPlayer.setOnEndOfMedia(runnable);
	}

	@Override
	public FileFilter getFilter() {
		return new FileNameExtensionFilter("Audio Files", "wav", "mp3", "aac");
	}

	@Override
	public PlayerState getState() {
		if(fxPlayer.getStatus() == MediaPlayer.Status.PLAYING) return PlayerState.PLAYING;
		else if(fxPlayer.getStatus() == MediaPlayer.Status.PAUSED) return PlayerState.PAUSED;
		else return PlayerState.INACTIVE;
	}

}
