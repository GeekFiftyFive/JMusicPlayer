import javax.swing.JOptionPane;

public class AboutAction extends PlayerRunnable{

	public AboutAction(JMusicPlayer player) {
		super(player);
	}

	@Override
	public void run() {
		JOptionPane.showMessageDialog(player, "JMusicPlayer 0.0.1", "About JMusicPlayer", JOptionPane.INFORMATION_MESSAGE);
	}

}
