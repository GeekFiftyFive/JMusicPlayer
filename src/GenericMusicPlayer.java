import javax.swing.filechooser.FileFilter;

import javafx.util.Duration;

public interface GenericMusicPlayer {
	public enum PlayerState {INACTIVE, PLAYING, PAUSED};
		
	void play();
	
	void pause();
	
	Duration getTotalDuration();
	
	Duration getCurrentDuration();
	
	void setCurrentDuration(Duration duration);
	
	void close();
	
	void setOnReady(Runnable runnable);
	
	void setOnEndOfMedia(Runnable runnable);
	
	PlayerState getState();
	
	FileFilter getFilter();
}
