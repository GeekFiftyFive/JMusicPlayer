import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.Collator;
import java.util.Collection;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;

import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.freedesktop.gstreamer.Gst;
import org.gagravarr.tika.FlacParser;

import java.util.Map;
import java.util.TreeSet;

import javafx.scene.media.Media;
import javafx.util.Duration;

public class JMusicPlayer extends JFrame implements ActionListener, ChangeListener, MouseListener{
	private static final long serialVersionUID = 1L;
	
	private JPanel panel;
	private JButton play;
	private JMenuBar menuBar;
	private JFileChooser fc;
	private JSlider scrubber;
	private JLabel scrubPos;
	private JLabel scrubLeft;
	private JTable table;
	private DefaultTableModel tableModel;
	
	private ImageIcon playIco;
	private ImageIcon pauseIco;
		
	private volatile GenericMusicPlayer player;
	private static GenericMusicPlayer emptyPlayer;
	
	private Duration duration;
	private int currentIndex;
	
	private Map<JComponent, Runnable> actions;
		
	public JMusicPlayer(GenericMusicPlayer empty) {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		
		//Create map to map from UI elements to actions
		actions = new HashMap<JComponent, Runnable>();
		
		//Attempt to load play/pause icons
		play = new JButton();
		try {
			playIco = new ImageIcon(ImageIO.read(getClass().getResource("icons/play.png")));
			pauseIco = new ImageIcon(ImageIO.read(getClass().getResource("icons/pause.png")));
			play.setIcon(playIco);
		} catch(IOException ex) {
			ex.printStackTrace();
			play.setText("Play");
		}
		
		//Add action listener to play button, and add it to the actions map
		play.addActionListener(this);
		actions.put(play, new PlayAction(this));
		
		//Create next and previous buttons and add them to the actions map
		JButton next = new JButton();
		next.addActionListener(this);
		actions.put(next, new MoveAction(this, 1));
		
		JButton prev = new JButton();
		prev.addActionListener(this);
		actions.put(prev,  new MoveAction(this, -1));
		
		//Attempt to load icons for next and previous icons
		ImageIcon nextIco, prevIco;
		try{
			nextIco = new ImageIcon(ImageIO.read(getClass().getResource("icons/next.png")));
			prevIco = new ImageIcon(ImageIO.read(getClass().getResource("icons/prev.png")));
			next.setIcon(nextIco);
			prev.setIcon(prevIco);
		} catch(IOException ex) {
			ex.printStackTrace();
			next.setText("Next");
			prev.setText("Prev");
		}
		
		//Create menu bar
		menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenu playlistMenu = new JMenu("Playlist");
		menuBar.add(playlistMenu);
		
		JMenu sortMenu = new JMenu("Sort");
		menuBar.add(sortMenu);
		
		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		//Add items to file menu
		this.addMenuItem(fileMenu, "Open", new OpenAction(this));
		this.addMenuItem(fileMenu, "Add to Playlist", new AddAction(this));
		this.addMenuItem(fileMenu, "Save Playlist", new SaveAction(this));
		
		//Add items to sorting menu
		this.addMenuItem(sortMenu, "Ascending", null);
		this.addMenuItem(sortMenu, "Title", new SortAction(this, SortAction.SortCriteria.Title, SortAction.SortOrder.Ascending));
		this.addMenuItem(sortMenu, "Artist", new SortAction(this, SortAction.SortCriteria.Artist, SortAction.SortOrder.Ascending));
		this.addMenuItem(sortMenu, "Album", new SortAction(this, SortAction.SortCriteria.Album, SortAction.SortOrder.Ascending));
		this.addMenuItem(sortMenu, "Genre", new SortAction(this, SortAction.SortCriteria.Genre, SortAction.SortOrder.Ascending));
		this.addMenuItem(sortMenu, "Decending", null);
		this.addMenuItem(sortMenu, "Title", new SortAction(this, SortAction.SortCriteria.Title, SortAction.SortOrder.Descending));
		this.addMenuItem(sortMenu, "Artist", new SortAction(this, SortAction.SortCriteria.Artist, SortAction.SortOrder.Descending));
		this.addMenuItem(sortMenu, "Album", new SortAction(this, SortAction.SortCriteria.Album, SortAction.SortOrder.Descending));
		this.addMenuItem(sortMenu, "Genre", new SortAction(this, SortAction.SortCriteria.Genre, SortAction.SortOrder.Descending));
		
		//Add items to playlist menu
		this.addMenuItem(playlistMenu, "Move Track Up", new MoveTrackAction(this, -1));
		this.addMenuItem(playlistMenu, "Move Track Down", new MoveTrackAction(this, 1));
		this.addMenuItem(playlistMenu, "", null);
		this.addMenuItem(playlistMenu, "Clear Playlist", new ClearAction(this));
		
		//Add items to help menu
		this.addMenuItem(helpMenu, "About JMusicPlayer", new AboutAction(this));
		
		//Create control slider and elapsed/remaining time indicators
		scrubber = new JSlider(0, 0, 0);
		scrubber.addChangeListener(this);
		
		scrubPos = new JLabel("--:--");
		scrubLeft = new JLabel("--:--");
		
		tableModel = createTableModel();
		table = new JTable(tableModel);
		
		JScrollPane tableScroll = new JScrollPane(table);
		table.addMouseListener(this);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		GridBagConstraints c = new GridBagConstraints();
		//Buttons doesn't resize
		c.fill = GridBagConstraints.NONE;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0;
		panel.add(prev, c);
		c.gridx++;
		panel.add(play, c);
		c.gridx++;
		panel.add(next, c);
		c.gridx++;
		panel.add(scrubPos, c);
		//Slider resizes
		c.gridx++;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		panel.add(scrubber, c);
		c.fill = GridBagConstraints.NONE;
		c.gridx++;
		c.weightx = 0;
		panel.add(scrubLeft, c);
		int width = c.gridx;
		//Table or tracks resizes with window
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = width + 1;
		panel.add(tableScroll, c);

		this.add(panel);
		this.setJMenuBar(menuBar);
				
		fc = new JFileChooser();
		emptyPlayer = empty;
		fc.setFileFilter(emptyPlayer.getFilter());
				
		currentIndex = 0;
	}
	
	private DefaultTableModel createTableModel() {
		String columnNames[] = { "Title", "Artist", "Album", "Genre"};
		String rowData[][] = {};
		
		DefaultTableModel model = new DefaultTableModel(rowData, columnNames) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2666706609873780011L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		return model;
	}
	
	public FileFilter getFilter() {
		return emptyPlayer.getFilter();
	}
	
	public JFileChooser getFileChooser() {
		return fc;
	}
	
	public DefaultTableModel getTableModel() {
		return tableModel;
	}
	
	@Override
	public void setTitle(String title) {
		String baseString = "JMusicPlayer";
		if(title == null) super.setTitle(baseString);
		else super.setTitle(baseString + " - " + title);
	}
	
	private void addMenuItem(JMenu menu, String text, Runnable runnable) {
		JMenuItem item = new JMenuItem(text);
		item.addActionListener(this);
		menu.add(item);
		actions.put(item, runnable);
		if(runnable == null) item.setEnabled(false);
	}
	
	public static String toTimecode(int time) {
		int secs = time % 60;
		int mins = time / 60;
		String secString;
		if(secs < 10) secString = new StringBuilder().append("0").append(secs).toString();
		else secString = new String(Integer.toString(secs));
		return new StringBuilder().append(mins).append(new String(":")).append(secString).toString();
	}
	
	public void tick() {
		//Check if a track is currently loaded
		if(player != null && player.getState() != GenericMusicPlayer.PlayerState.INACTIVE) {
			duration = player.getTotalDuration();
			scrubber.setMaximum((int) duration.toSeconds());
			//Update the slider position and elapsed time/remaining time indicators
			int location = (int) player.getCurrentDuration().toSeconds();
			if(!scrubber.getValueIsAdjusting()) scrubber.setValue(location);
			int remaining = (int) duration.toSeconds() - location;
			scrubPos.setText(toTimecode(location));
			scrubLeft.setText(toTimecode(remaining));
		}
	}

	public static void main(String[] args) {
		GenericMusicPlayer empty;
		//Check command line arguments
		if(args.length >= 1 && args[0].equals("-nogst")) {
			//Use JavaFX backend
		    new javafx.embed.swing.JFXPanel(); // forces JavaFX init
		    empty = new JavaFXMusicPlayer();
		} else {
			//Use GStreamer backend
			Gst.init();
			empty = new GStreamerMusicPlayer();
		}
		EventQueue.invokeLater(() -> {
			JMusicPlayer player = new JMusicPlayer(empty);
			player.setTitle(null);
			player.setSize(400, 400);
			player.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			//Load default playlist, stored at ~/.defaultPlaylist.jmp on *nix-like systems
			File defaultPlaylist = new File(System.getProperty("user.home") + System.getProperty("file.separator") + ".default_playlist.jmp");
			PlaylistParser.parsePlaylist(player, defaultPlaylist);
		
			//Create a new timer to update slider and time indicators
			new Timer(50, e -> {
				player.tick();
			}).start();
			
			player.setVisible(true);
		});
	}
	
	public void moveTrack(int delta) {
		int size = tableModel.getRowCount();
		if(size == 0) return;
		//Update the selected row, wraps around at beginning and end of playlist
		currentIndex = (currentIndex + delta + size) % size;
		//Select new row
		table.setRowSelectionInterval(currentIndex, currentIndex);
		int selectedRow = table.getSelectedRow();
		//Get the TitlePath of the selected row (this stores the path the track)
		TitlePath selected = (TitlePath) tableModel.getValueAt(selectedRow, 0);
		if(playTrack(selected.getPath(), true)) {
			player.play();
			play.setIcon(pauseIco);
			setTitle(tableModel.getValueAt(selectedRow, 0).toString());
		}
	}
	
	public void moveInPlaylist(int delta) {
		int size = tableModel.getRowCount(); 
		if(size == 0) return;
		int index = table.getSelectedRow();
		Object row[] = new Object[4];
		
		//Get the row data of the currently selected row
		for(int i = 0; i < 4; i++) row[i] = tableModel.getValueAt(index, i);
		
		//Remove the row at its old position
		tableModel.removeRow(index);
		//Find new position (wraps around at beginning and end of playlist)
		int newIndex = (index + delta + size) % size;
		//Insert row data at now index
		tableModel.insertRow(newIndex, row);
		
		//Select row at new index
		table.setRowSelectionInterval(newIndex, newIndex);
	}
	
	public void clearPlaylist() {
		tableModel = createTableModel();
		table.setModel(tableModel);
	}
	
	public void addToPlaylist(File file) {
		Parser parse;
		//Get the file extension to determine the metadata parser to be used
		int extIndex = file.getName().lastIndexOf(".") + 1;
		String ext = file.getName().substring(extIndex, file.getName().length());
		
		//Choose the appropriate metadata parser
		if(ext.equals("mp3")) parse = new Mp3Parser();
		else if(ext.equals("flac")) parse = new FlacParser();
		else parse = null;
		
		//Parse the metadata
		MetaDataParser parser = new MetaDataParser(file, parse);
		try {
			parser.parse();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Create new title path (this stores the title and the path of the track)
		TitlePath titlePath = new TitlePath(parser.getTitle(), file.toURI().toString());
		
		//Add the metadata to the table
		tableModel.addRow(new Object[] {titlePath, parser.getArtist(), parser.getAlbum(), parser.getGenre()});
	}
	
	public void sortTracks(SortAction.SortCriteria crit, SortAction.SortOrder order) {
		int col = -1;
		//Choose the column to sort by
		switch(crit) {
			case Title:
				col = 0;
				break;
			case Artist:
				col = 1;
				break;
			case Album:
				col = 2;
				break;
			case Genre:
				col = 3;
				break;
		}
		
		//Create a TreeSet to sort the tracks
		Collection<String> tracks = new TreeSet<String>(Collator.getInstance());
		
		//Add data from the selected column to the TreeSet
		for(int i = 0; i < tableModel.getRowCount(); i++) {
			String toAdd = new String(tableModel.getValueAt(i, col).toString());
			//Append a null character followed by the original index in the table to the string 
			tracks.add((new StringBuilder(toAdd)).append('\0').append(i).toString());
		}
		
		//Create a new table to store the sorted rows
		DefaultTableModel sortedTable = createTableModel();
		
		for(String track : tracks) {
			Object rowData[] = new Object[4];
			int terminator = track.lastIndexOf('\0');
			//Get the original index of the track
			int uid = Integer.parseInt(track.substring(terminator + 1));
			//Populate rowData
			for(int i = 0; i < 4; i++) rowData[i] = tableModel.getValueAt(uid, i);
			//Add track to new table, the position is dependent on whether ascending or descending
			//order is to be used
			if(order == SortAction.SortOrder.Ascending) sortedTable.addRow(rowData);
			else sortedTable.insertRow(0, rowData);
		}
		
		table.setModel(sortedTable);
		tableModel = sortedTable;
	}
	
	public Boolean playTrack(String uri, Boolean playlistMode) {
		try {
			//Close the existing player, if it exists
			if(player != null && player.getState() == GenericMusicPlayer.PlayerState.PLAYING) player.close();
			
			//Get the correct backend
			if(emptyPlayer.getClass() == new GStreamerMusicPlayer().getClass()) player = new GStreamerMusicPlayer(new URI(uri));
			else player = new JavaFXMusicPlayer(new Media(uri));
			
			//Reset the controls
			scrubber.setValue(0);
			play.setIcon(playIco);
			
			//Add callback to get duration of track for controls
			player.setOnReady(new Runnable() {
				@Override
				public void run() {
					duration = player.getTotalDuration();
					scrubber.setMaximum((int) duration.toSeconds());
				}
			});
			
			//If in playlist mode, rather than single track mode, add a
			//callback when the track finishes to move to the next track
			if(playlistMode) {
				player.setOnEndOfMedia(new Runnable() {

					@Override
					public void run() {
						moveTrack(1);
					}
				
				});
			} else {
				player.setOnEndOfMedia(new Runnable() {

					@Override
					public void run() {
						play.setIcon(playIco);
					}
					
				});
			}
			
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	
	public void playMethod() {
		if(player == null) {
			//If no track if currently playing, get the selected track from the table and play that
			int index = table.getSelectedRow();
			TitlePath selected = (TitlePath) tableModel.getValueAt(index, 0);
			if(playTrack(selected.getPath(), true)) {
				this.setTitle(selected.toString());
				currentIndex = index;
			}
		}
		
		//Update icons
		if(player.getState() == GenericMusicPlayer.PlayerState.PLAYING) {
			player.pause();
			play.setIcon(playIco);
		} else {
			player.play();
			play.setIcon(pauseIco);
		}	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			//Call the action in the actions map that corresponds to the event source
			actions.get(e.getSource()).run();
		} catch(NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() == scrubber) {
			//If the user is moving the scrubber, update the track position
			if(player != null && player.getState() != GenericMusicPlayer.PlayerState.INACTIVE && scrubber.getValueIsAdjusting()) {
				player.setCurrentDuration(Duration.seconds(scrubber.getValue()));
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//If table is double clicked, play selected track in table
		if(e.getSource() == table && e.getClickCount() == 2) {
			if(tableModel.getRowCount() == 0) return;
			if(player != null) player.close();
			int index = table.getSelectedRow();
			TitlePath selected = (TitlePath) tableModel.getValueAt(index, 0);
			if(playTrack(selected.getPath(), true)) {
				this.setTitle(selected.toString());
				player.play();
				play.setIcon(pauseIco);
				currentIndex = index;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
