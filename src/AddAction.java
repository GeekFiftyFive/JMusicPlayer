import java.io.File;

import javax.swing.JFileChooser;

public class AddAction extends PlayerRunnable {

	public AddAction(JMusicPlayer player) {
		super(player);
	}
	
	@Override
	public void run() {
		player.getFileChooser().setMultiSelectionEnabled(true);
		int returnVal = player.getFileChooser().showOpenDialog(player);
		if(returnVal != JFileChooser.APPROVE_OPTION) return;
		File files[] = player.getFileChooser().getSelectedFiles();
		
		for(File file : files) {
			player.addToPlaylist(file);
		}
	}

}
