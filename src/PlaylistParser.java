import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public abstract class PlaylistParser {
	public static void parsePlaylist(JMusicPlayer player, File file) {
		try {
			BufferedReader r = new BufferedReader(new FileReader(file));
			String s = r.readLine();
			while (s != null) {
				File track;
				try {
					track = new File((new URI(s)).getPath());
				} catch (URISyntaxException e) {
					track = null;
					e.printStackTrace();
				}
				player.addToPlaylist(track);
			    s = r.readLine();
			}
			r.close();
		
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}
