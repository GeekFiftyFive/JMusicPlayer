
public class SortAction extends PlayerRunnable {
	public enum SortCriteria{Title, Artist, Album, Genre};
	public enum SortOrder{Ascending, Descending};
	private SortCriteria crit;
	private SortOrder order;

	public SortAction(JMusicPlayer player, SortCriteria crit, SortOrder order) {
		super(player);
		this.crit = crit;
		this.order = order;
	}

	@Override
	public void run() {
		player.sortTracks(crit, order);
	}

}
