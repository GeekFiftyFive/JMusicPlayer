import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class SaveAction extends PlayerRunnable{
	
	public SaveAction(JMusicPlayer player) {
		super(player);
	}

	@Override
	public void run() {
		JFileChooser fc = player.getFileChooser();
		fc.setMultiSelectionEnabled(false);
		fc.setFileFilter(new FileNameExtensionFilter("JMusicPlayer Playlist", "jmp"));
		fc.setSelectedFile(new File(System.getProperty("user.home") + System.getProperty("file.separator") + ".default_playlist.jmp"));

		int returnVal = fc.showSaveDialog(player);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				FileWriter file = new FileWriter(fc.getSelectedFile());
				DefaultTableModel tm = player.getTableModel();
			
				for(int i = 0; i < tm.getRowCount(); i++) {
					String path = ((TitlePath) tm.getValueAt(i, 0)).getPath();
					file.write(path + '\n');
				}
			
				file.close();
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		
		fc.setFileFilter(player.getFilter());
		fc.setSelectedFile(null);
	}
}
