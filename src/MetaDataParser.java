import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MetaDataParser {
	private File file;
	Parser parser;
	
	private String title;
	private String artist;
	private String album;
	private String genre;
	
	public MetaDataParser(File file, Parser parser){
		this.file = file;
		this.parser = parser;
	}

	void parse() throws FileNotFoundException, IOException {
		if(parser == null) return;
		BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
		ContentHandler handler = new DefaultHandler();
		Metadata metadata = new Metadata();
		ParseContext ctx = new ParseContext();
		try {
			parser.parse(stream, handler, metadata, ctx);
		} catch(TikaException ex) {
			ex.printStackTrace();
			return;
		} catch(SAXException ex) {
			ex.printStackTrace();
			return;
		}
				
		title = metadata.get("title");
		artist = metadata.get("xmpDM:artist");
		album = metadata.get("xmpDM:album");
		genre = metadata.get("xmpDM:genre");
	}

	String getTitle() {
		if(title == null) return "";
		return title;
	}

	String getArtist() {
		if(artist == null) return "";
		return artist;
	}

	String getAlbum() {
		if(album == null) return "";
		return album;
	}

	String getGenre() {
		if(genre == null) return "";
		return genre;
	}
	
}
