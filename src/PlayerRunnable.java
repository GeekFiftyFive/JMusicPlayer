
public abstract class PlayerRunnable implements Runnable {
	JMusicPlayer player;
	
	public PlayerRunnable(JMusicPlayer player) {
		this.player = player;
	}
}
