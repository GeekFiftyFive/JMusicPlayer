

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.table.DefaultTableModel;

import org.junit.jupiter.api.Test;

class TestSort {
	private Object[][] getData() {
		Object data[][] = new Object[4][4];
		data[0] = new Object[]{"A song", "B artist", "C album", "D genre"};
		data[1] = new Object[]{"B song", "C artist", "D album", "A genre"};
		data[2] = new Object[]{"C song", "D artist", "A album", "B genre"};
		data[3] = new Object[]{"D song", "A artist", "B album", "C genre"};
		return data;
	}

	@Test
	void testSortTitle() {
		JMusicPlayer player = new JMusicPlayer(new JavaFXMusicPlayer());
		DefaultTableModel tm = player.getTableModel();
		Object[][] data = getData();
		tm.addRow(data[0]);
		tm.addRow(data[1]);
		tm.addRow(data[2]);
		tm.addRow(data[3]);
		
		player.sortTracks(SortAction.SortCriteria.Title, SortAction.SortOrder.Descending);
		tm = player.getTableModel();
		int i = 0;
		if(tm.getValueAt(0, 0).equals("D song")) i++;
		if(tm.getValueAt(1, 0).equals("C song")) i++;
		if(tm.getValueAt(2, 0).equals("B song")) i++;
		if(tm.getValueAt(3, 0).equals("A song")) i++;
		if(i != 4) fail("Titles didn't sort in descending order");
	}
	
	@Test
	void testSortGenre() {
		JMusicPlayer player = new JMusicPlayer(new JavaFXMusicPlayer());
		DefaultTableModel tm = player.getTableModel();
		Object[][] data = getData();
		tm.addRow(data[0]);
		tm.addRow(data[1]);
		tm.addRow(data[2]);
		tm.addRow(data[3]);
		
		player.sortTracks(SortAction.SortCriteria.Genre, SortAction.SortOrder.Ascending);
		tm = player.getTableModel();
		int i = 0;
		if(tm.getValueAt(0, 3).equals("A genre")) i++;
		if(tm.getValueAt(1, 3).equals("B genre")) i++;
		if(tm.getValueAt(2, 3).equals("C genre")) i++;
		if(tm.getValueAt(3, 3).equals("D genre")) i++;
		if(i != 4) fail("Genres didn't sort in ascending order");
	}

}
