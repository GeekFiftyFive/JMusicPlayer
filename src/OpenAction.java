import java.io.File;

import javax.swing.JFileChooser;

public class OpenAction extends PlayerRunnable {
	
	public OpenAction(JMusicPlayer player) {
		super(player);
	}

	@Override
	public void run() {
		player.getFileChooser().setMultiSelectionEnabled(false);
		int returnVal = player.getFileChooser().showOpenDialog(player);
		if(returnVal != JFileChooser.APPROVE_OPTION) return;
		File file = player.getFileChooser().getSelectedFile();
		
		if(player.playTrack(file.toURI().toString(), false)) player.setTitle("JMusicPlayer - " + file.getName());
	}

}
