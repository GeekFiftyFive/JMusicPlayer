
public class PlayAction extends PlayerRunnable {
	
	public PlayAction(JMusicPlayer player) {
		super(player);
	}
	
	@Override
	public void run() {
		player.playMethod();
	}

}
