
public class ClearAction extends PlayerRunnable{

	public ClearAction(JMusicPlayer player) {
		super(player);
	}

	@Override
	public void run() {
		player.clearPlaylist();
	}

}
