
public class TitlePath {
	private String title;
	private String path;
	
	public TitlePath(String title, String path) {
		this.title = title;
		this.path = path;
	}
	
	String getPath() {
		return path;
	}
	
	@Override
	public String toString() {
		return title;
	}
	
}
