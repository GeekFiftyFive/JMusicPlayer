# JMusicPlayer
Simple music player written in Java.

## Dependencies
 * Apache Tika
 * Gstreamer Java Bindings
 * JNA
 * OpenJFX

## Using JavaFX backend
In order to use the JavaFX backend rather than the Gstreamer one, pass the argument -nogst
